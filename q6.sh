#!/bin/bash
read salary
k=$(echo "scale=4;12.0*$salary" | bc)
if (($(bc <<< "$k > 300000")))
	
then
	l=$(echo "scale=4;3.0*$k/10" | bc)
	echo "TAX PAYMENT REQUIRED:$l"
else
	echo "NO TAX PAYMENT REQUIRED"
fi	

